package com.aerors.sbgt.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.aerors.sbgt.service.WMSService;

@CrossOrigin
@RestController
@RequestMapping("/wms")
public class WMSController {
	@Autowired
	private  WMSService wmsService;

    @RequestMapping("/greeting")
    public String greeting() {
        return  "hello";
    }
	
	@RequestMapping("/getMap")
	public ResponseEntity<byte[]> getMap(@RequestParam("BBOX")String bbox,
			@RequestParam("WIDTH")String width,
			@RequestParam("HEIGHT")String height,
			@RequestParam("LAYERS")String layer) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.IMAGE_PNG);
		return new ResponseEntity<byte[]>(
				wmsService.getMap(bbox, width, height, layer), 
				headers, HttpStatus.OK
				);
	}
	
	@RequestMapping("/getInfo")
	public void getInfo() {
		
	}
}
