package com.aerors.sbgt;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.aerors.sbgt.dao.CustomerRepository;
import com.aerors.sbgt.dao.RasterLayerInfoRepository;
import com.aerors.sbgt.entity.Customer;
import com.aerors.sbgt.entity.RasterLayerInfo;

@SpringBootApplication
public class SbgtApplication {
	private static final Logger log = LoggerFactory.getLogger(SbgtApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(SbgtApplication.class, args);
	}
	
/*	@Bean
	public CommandLineRunner dataInit(RasterLayerInfoRepository repository) {
		return (args)->{
//			repository.save(new RasterLayerInfo("2011", "D:\\智慧城管\\haerbin\\2011_output_cai.tif"));
//			repository.save(new RasterLayerInfo("2017", "D:\\智慧城管\\haerbin\\2017cai.tif"));
			// fetch all customers
			log.info("Customers found with findAll():");
			log.info("-------------------------------");
			for (RasterLayerInfo raster : repository.findAll()) {
				log.info(raster.toString());
			}
			log.info("");
		};
	}*/
	
/*	@Bean
	public CommandLineRunner demo(CustomerRepository repository) {
		return (args) -> {
			// save a couple of customers
			repository.save(new Customer("Jack", "Bauer"));
			repository.save(new Customer("Chloe", "O'Brian"));
			repository.save(new Customer("Kim", "Bauer"));
			repository.save(new Customer("David", "Palmer"));
			repository.save(new Customer("Michelle", "Dessler"));
			// fetch all customers
			log.info("Customers found with findAll():");
			log.info("-------------------------------");
			for (Customer customer : repository.findAll()) {
				log.info(customer.toString());
			}
			log.info("");

			// fetch an individual customer by ID
			Customer customer = repository.findOne(1L);
			log.info("Customer found with findOne(1L):");
			log.info("--------------------------------");
			log.info(customer.toString());
			log.info("");

			// fetch customers by last name
			log.info("Customer found with findByLastName('Bauer'):");
			log.info("--------------------------------------------");
			for (Customer bauer : repository.findByLastName("Bauer")) {
				log.info(bauer.toString());
			}
			log.info("");
		};
	}*/
}
