package com.aerors.sbgt.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.aerors.sbgt.entity.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Long>{
	List<Customer> findByLastName(String lastName);
}
