package com.aerors.sbgt.entity;

public class TileInfo {
	public String rowNumber;
	public String columnNumber;
	public String scale;
	public String hexRowNumber;
	public String hexColumnNumber;
	public BoundingBox bbox;

	@Override
	public String toString() {
		return "TileMatrix [rowNumber=" + rowNumber + ", columnNumber=" + columnNumber + ", scale=" + scale
				+ ", hexRowNumber=" + hexRowNumber + ", hexColumnNumber=" + hexColumnNumber + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((columnNumber == null) ? 0 : columnNumber.hashCode());
		result = prime * result + ((hexColumnNumber == null) ? 0 : hexColumnNumber.hashCode());
		result = prime * result + ((hexRowNumber == null) ? 0 : hexRowNumber.hashCode());
		result = prime * result + ((rowNumber == null) ? 0 : rowNumber.hashCode());
		result = prime * result + ((scale == null) ? 0 : scale.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TileInfo other = (TileInfo) obj;
		if (columnNumber == null) {
			if (other.columnNumber != null)
				return false;
		} else if (!columnNumber.equals(other.columnNumber))
			return false;
		if (hexColumnNumber == null) {
			if (other.hexColumnNumber != null)
				return false;
		} else if (!hexColumnNumber.equals(other.hexColumnNumber))
			return false;
		if (hexRowNumber == null) {
			if (other.hexRowNumber != null)
				return false;
		} else if (!hexRowNumber.equals(other.hexRowNumber))
			return false;
		if (rowNumber == null) {
			if (other.rowNumber != null)
				return false;
		} else if (!rowNumber.equals(other.rowNumber))
			return false;
		if (scale == null) {
			if (other.scale != null)
				return false;
		} else if (!scale.equals(other.scale))
			return false;
		return true;
	}
}
